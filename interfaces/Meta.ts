export default interface IMetaOptions {
    title: string;
    description: string;
    image?: string;
}