import { ICmsImage } from "./ICmsImage";

export declare interface ICmsImageGrid {
    Images: ICmsImage[];
}