export declare interface ICmsPage {
    id: number;
    content: {
        Title: string;
        description: string;
        Seo_image: string;
        Text?:string;
        content: {
            component: string;
        }
    }
}