export declare interface ICmsImage {
    filename: string;
    alt: string;
    fieldtype: string;
    name: string;
    title: string;
    id: number;
}