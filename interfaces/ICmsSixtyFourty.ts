import { ICmsBlock } from "./ICmsBlock";
import { ICmsImage } from "./ICmsImage";

export declare interface ICmsSixtyFourty extends ICmsBlock{
    Image:ICmsImage;
}