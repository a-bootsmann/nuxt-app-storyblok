import { ICmsBlock } from "./ICmsBlock";
import { ICmsImage } from "./ICmsImage";

export declare interface ICmsVideo extends ICmsBlock {
    Headline: string;
    Teaser: string;
    Video: ICmsImage;
}