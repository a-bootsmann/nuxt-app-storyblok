export declare interface ICmsBlock {
    Background_color: string;
    Headline: string;
    Teaser?: string;
    Text?: string;
}