export declare interface ICmsLink {
    cached_url: string;
    url: string;
    fieldtype: string;
    linktype: string;
    title: string;
    id: string;
}