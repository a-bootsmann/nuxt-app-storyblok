import { ICmsImage } from "./ICmsImage";
import { ICmsLink } from "./ICmsLink";

export declare interface IHeroItem {
	_uid: string;
	Image: ICmsImage;
	Headline: string;
	CTA: ICmsLink;
	CTA_Text: string;
}
