import { StoryblokVue, apiPlugin } from "@storyblok/vue";

export default defineNuxtPlugin((nuxtApp) => {
	// Doing something with nuxtApp
	nuxtApp.vueApp.use(StoryblokVue, {
		accessToken: nuxtApp.payload.config.STORYBLOK_API_TOKEN,
		use: [apiPlugin],
	});
});
