import IMetaOptions from "../interfaces/Meta";

export default (data: IMetaOptions) => {
    useMeta(() => {
        return {
            title: data.title,
            meta: [
                {
                    name: "description", content:  data.description,
                },
                {
                    name: "image", content: data.image != null ? data.image : "",
                }
            ]
        }
    })

    return;
}