export default (value: string) => {
    switch (value) {
        case "Image grid":
            return "ImageGrid";
        case "Single image":
            return "SingleImage";
        case "60_40":
            return "SixtyFourty";
        default:
            return value;
    }
}