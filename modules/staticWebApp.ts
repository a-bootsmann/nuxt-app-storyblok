import { defineNuxtModule } from "@nuxt/kit";
import fs from "fs";
export default defineNuxtModule({
	meta: {
		name: "staticWebApp",
		version: "0.0.1",
		configKey: "staticWebApp",
		compatibility: {
			bridge: false,
		},
	},
	defaults: {},
	async setup(options, nuxt) {
		nuxt.hook("build:done", async (builder) => {
			const filePath = `${__dirname}/../.output/public/staticwebapp.config.json`;
			if (fs.existsSync(filePath)) {
				const jsonString = await fs.promises.readFile(filePath, "utf8");
				const object = JSON.parse(jsonString);
				object.platform = { apiRuntime: "node:12" };
				await fs.promises.writeFile(filePath, JSON.stringify(object));
			}
		});
	},
});
