import { defineNuxtConfig } from "nuxt3";

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
	css: ["@/assets/styles/site.scss"],
	meta: {
		link: [
			{
				rel: "preconnect",
				href: "https://fonts.googleapis.com",
			},
			{
				rel: "preconnect",
				href: "https://fonts.gstatic.com",
				crossorigin: true,
			},
			{
				rel: "stylesheet",
				async: true,
				href: "https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap",
			},
		],
	},
	publicRuntimeConfig: {
		STORYBLOK_API_TOKEN: process.env.STORYBLOK_API_TOKEN,
	},
	components: {
		global: true,
		dirs: ["~/components", "~/Storyblok/components", "~/Storyblok/pages"],
	},
	modules: [
		 "~/modules/staticWebApp"
	],
	generate: {},
	// vite: {
	// 	build: {
	// 		rollupOptions: {
	// 			external: ["axios"],
	// 		},
	// 	},
	// },
});
